require 'serverspec'
set :backend, :exec

ocs_dirs = ['/etc/ocsinventory-agent', '/var/lib/ocsinventory-agent']
ocs_files = ['/etc/cron.d/ocsinventory-agent']

redhat_files = "/usr/local/lib64/perl5/auto/Ocsinventory/Unix/Agent/.packlist"

if os[:family] == 'ubuntu'
  perl_version = `perl -e 'print $^V' | cut -c2-`
  perl_version = perl_version.strip
  ubuntu_files = "/usr/local/lib/perl/#{perl_version}/auto/Ocsinventory/Unix/Agent/.packlist"
end

def create_array_from_file(path_file, ocs_files)
  perl_files = IO.readlines(path_file)
  perl_files.each { |line| line.strip! }
  return ocs_files + perl_files
end

if os[:family] == 'redhat'
  files = create_array_from_file(redhat_files, ocs_files)
elsif ['ubuntu'].include?(os[:family])
  files = create_array_from_file(ubuntu_files, ocs_files)
end

describe 'Test if the agent dirs are installed' do
  ocs_dirs.each do |directory|
    describe file("#{directory}") do
      it { should be_directory }
    end
  end
end

describe 'Test if the agent files are installed' do
  files.each do |file|
    describe file("#{file}") do
      it { should exist }
    end
  end
end

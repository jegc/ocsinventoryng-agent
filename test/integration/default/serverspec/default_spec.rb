require 'serverspec'
set :backend, :exec

redhat_pkgs = ['perl-XML-Simple', 'perl-Net-IP', 'perl-Net-SSLeay', 'perl-Crypt-SSLeay', 'perl-Net-SNMP', 'perl-Proc-Daemon', 'perl-Proc-PID-File', 'perl-Digest-Perl-MD5', 'perl-libwww-perl', 'perl-ExtUtils-MakeMaker', 'perl-Compress-Raw-Zlib', 'perl-LWP-Protocol-https', 'perl-Digest-MD5', 'perl-Sys-Syslog', 'perl-Net-CUPS', 'net-tools', 'pciutils', 'dmidecode', 'smartmontools', 'monitor-edid', 'make', 'gcc']

ubuntu_pkgs = ['dmidecode', 'libxml-simple-perl', 'libnet-ip-perl', 'libwww-perl', 'libnet-ssleay-perl', 'libcrypt-ssleay-perl', 'libnet-snmp-perl', 'libproc-pid-file-perl', 'libproc-daemon-perl', 'libnet-netmask-perl', 'net-tools', 'libsys-syslog-perl', 'pciutils', 'hdparm', 'smartmontools', 'read-edid', 'make']

if os[:family] == 'redhat'
  packages = redhat_pkgs
elsif ['ubuntu'].include?(os[:family])
  packages = ubuntu_pkgs
end

describe 'Test if packages are installed' do
  packages.each do |pkg|
    describe package("#{pkg}") do
      it { should be_installed }
    end
  end
end
